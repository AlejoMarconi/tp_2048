package interfazJuego;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;

import controlador.ControladorJuego;

public class Interfaz {
    private JFrame frame;
    private JPanel tablero;
    private JLabel[][] casillas;
    private static final int TAMANIO = 4;
    private ControladorJuego controlador;

    public Interfaz() {
        frame = new JFrame("2048");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());

        tablero = new JPanel();
        tablero.setLayout(new GridLayout(TAMANIO, TAMANIO));
        frame.getContentPane().add(tablero, BorderLayout.CENTER);

        frame.setVisible(true);
    }

    public void setTablero(int[][] matriz) {
        casillas = new JLabel[TAMANIO][TAMANIO];
        for (int i = 0; i < TAMANIO; i++) {
            for (int j = 0; j < TAMANIO; j++) {
                casillas[i][j] = new JLabel();
                casillas[i][j].setHorizontalAlignment(SwingConstants.CENTER);
                casillas[i][j].setOpaque(true);
                casillas[i][j].setBackground(Color.LIGHT_GRAY);
                casillas[i][j].setFont(new Font("Arial", Font.BOLD, 24));
                tablero.add(casillas[i][j]);
            }
        }
        actualizarTablero(matriz);
    }

    public void actualizarTablero(int[][] tablero) {
        for (int i = 0; i < TAMANIO; i++) {
            for (int j = 0; j < TAMANIO; j++) {
                casillas[i][j].setText(tablero[i][j] == 0 ? "" : String.valueOf(tablero[i][j]));
                Color color;
                switch (tablero[i][j]) {
                case 0: color = Color.LIGHT_GRAY; break;
                case 2: color = new Color(0xEEE4DA); break;
                case 4: color = new Color(0xEDE0C8); break;
                case 8: color = new Color(0xF2B179); break;
                case 16: color = new Color(0xF59563); break;
                case 32: color = new Color(0xF67C5F); break;
                case 64: color = new Color(0xF65E3B); break;
                case 128: color = new Color(0xEDCF72); break;
                case 256: color = new Color(0xEDCC61); break;
                case 512: color = new Color(0xEDC850); break;
                case 1024: color = new Color(0xEDC53F); break;
                case 2048: color = new Color(0xEDC22E); break;
                default: color = Color.BLACK;
                }
                casillas[i][j].setBackground(color);
            }
        }
    }

    public void setControlador(ControladorJuego controlador) {
        this.controlador = controlador;
    }
    
    public void addKeyListener(KeyListener listener) {
        frame.addKeyListener(listener);
    }
}
