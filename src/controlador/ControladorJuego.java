package controlador;

import interfazJuego.Interfaz;
import logica.Tablero;

import java.awt.event.*;

public class ControladorJuego {
    private Tablero tablero;
    private Interfaz interfaz;

    public ControladorJuego(Interfaz interfaz) {
        this.interfaz = interfaz;
        this.tablero = new Tablero(16);
        interfaz.setTablero(tablero.getTablero());
    }

    public void moverIzquierda() {
        tablero.moverIzquierda();
        interfaz.actualizarTablero(tablero.getTablero());
    }

    public void moverDerecha() {
        tablero.moverDerecha();
        interfaz.actualizarTablero(tablero.getTablero());
    }

    public void moverArriba() {
        tablero.moverArriba();
        interfaz.actualizarTablero(tablero.getTablero());
    }

    public void moverAbajo() {
        tablero.moverAbajo();
        interfaz.actualizarTablero(tablero.getTablero());
    }

    public void manejarEvento(int codigoTecla) {
        switch (codigoTecla) {
            case KeyEvent.VK_LEFT:
                moverIzquierda();
                break;
            case KeyEvent.VK_RIGHT:
                moverDerecha();
                break;
            case KeyEvent.VK_UP:
                moverArriba();
                break;
            case KeyEvent.VK_DOWN:
                moverAbajo();
                break;
        }
    }
}