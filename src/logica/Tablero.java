package logica;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class Tablero {

    private ArrayList<HashSet<Integer>> vecinos;
    private int[][] valores;
    private int v;

    public static final int FILAS = 4;
    public static final int COLUMNAS = 4;

    // Constructor
    public Tablero(int n) {
        this.v = n;
        vecinos = new ArrayList<HashSet<Integer>>();
        for (int i = 0; i < n; ++i)
            vecinos.add(new HashSet<Integer>());

        valores = new int[FILAS][COLUMNAS];
        for (int i = 0; i < FILAS; ++i) {
            for (int j = 0; j < COLUMNAS; ++j) {
                valores[i][j] = 0; // Inicializar todos los valores de los casilleros como 0
            }
        }

//        this.crearTablero();
        
        PosicionarNumAleatorio();
        PosicionarNumAleatorio();
    }

    public void agregarArista(int i, int j){
        vecinos.get(i).add(j);
        vecinos.get(j).add(i);
    }

    public void eliminarArista(int i, int j) {
        vecinos.get(i).remove(j);
        vecinos.get(j).remove(i);
    }

    public boolean existeArista(int i, int j) {
        return vecinos.get(i).contains(j);
    }

    private void crearTablero(){
        for (int i = 0; i < v; ++i) {
            for (int j = 0; j < v; ++j) {
                if(i != j)
                    agregarArista(i,j);
            }
        }
    }


    public void imprimirGrafo() {
        for (int i=0; i<v; ++i) {
            System.out.println("Lista de adyacencia del vértice " + i);
            for (Integer nodo : vecinos.get(i)) {
                System.out.print(" -> " + nodo);
            }
            System.out.println("\n");
        }
    }



    public void agregarValor(int fila,int columna,int valor){
        valores[fila][columna] = valor;

        this.ady(fila, columna);

        //this.modificarMatrizAdy();
    }

    private void ady(int fila,int columna){
        //Adyacencia izq, si estoy en la primera columna, no hay izquierda
        if(columna != 0) {
            if (valores[fila][columna] == valores[fila][columna - 1] || valores[fila][columna] == 0 || valores[fila][columna - 1] == 0)
                agregarArista(fila * 4 + columna, fila * 4 + columna - 1);
            else
                eliminarArista(fila * 4 + columna, fila * 4 + columna - 1);
        }
        //Adyacencia derecha, si estoy en la ultima columna, no hay derecha
        if(columna != COLUMNAS - 1){
            if (valores[fila][columna] == valores[fila][columna + 1] || valores[fila][columna] == 0 || valores[fila][columna + 1] == 0)
                agregarArista(fila * 4 + columna, fila * 4 + columna + 1);
            else
                eliminarArista(fila * 4 + columna, fila * 4 + columna + 1);
        }

        //Adyacencia arriba, si esoty en la primera fila, no hay arriba
        if(fila != 0){
            if (valores[fila][columna] == valores[fila - 1][columna] || valores[fila][columna] == 0 || valores[fila - 1][columna] == 0)
                agregarArista(fila * 4 + columna, ((fila-1) * 4) + columna);
            else
                eliminarArista(fila * 4 + columna, ((fila-1) * 4) + columna);
        }

        //Adyacencia abajo, si esoty en la ultima fila, no hay abajo
        if(fila != FILAS - 1){
            if (valores[fila][columna] == valores[fila+1][columna] || valores[fila][columna] == 0 || valores[fila+1][columna] == 0)
                agregarArista(fila * 4 + columna, (fila+1) * 4 + columna);
            else
                eliminarArista(fila * 4 + columna, (fila+1) * 4 + columna);
        }

    }

    private void modificarMatrizAdy(){
        for (int i = 0; i < valores.length; i++) {
            for (int j = 0; j < valores[i].length; j++) {
                int elementoActual = valores[i][j];
                if (elementoActual != 0) {
                    for (int x = 0; x < valores.length; x++) {
                        for (int y = 0; y < valores[x].length; y++) {
                            if (x != i || y != j) { // Evita comparar el elemento consigo mismo
                                int otroElemento = valores[x][y];
                                if (elementoActual != otroElemento && otroElemento != 0) {
                                    this.eliminarArista(i * 4 + j,(x * 4 + y));
                                }else{
                                    this.agregarArista(i * 4 + j,x * 4 + y);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void imprimirValores(){
        for (int i = 0; i < FILAS; ++i) {
            for (int j = 0; j < COLUMNAS; ++j) {
                System.out.print(valores[i][j]+ " ");
            }
            System.out.println();
        }
    }



    public int moverDerecha(){
        System.out.println("MOVIENDO A LA DERECHA\n");
        boolean movimiento = false;
        for(int i = 0; i < FILAS; i++){
            int j = 2;
            while(j >= 0){
                if(existeArista(i*4+j,i*4+j+1) && valores[i][j] != 0){
                    valores[i][j+1] += valores[i][j];
                    valores[i][j] = 0;
                    this.ady(i,j+1);
                    this.ady(i,j);
                    movimiento = true;
                    if(j < COLUMNAS - 1)
                        j++;
                }else {
                    j--;
                }
            }
        }
        if (movimiento)
        	PosicionarNumAleatorio();
        
        return 0;
    }

    public int moverIzquierda(){
        boolean movimiento = false;
        for(int i = 0; i < FILAS; i++){
            int j = 1;
            while(j < COLUMNAS){
                if(existeArista(i*4+j,i*4+j-1) && valores[i][j] != 0){
                    valores[i][j-1] += valores[i][j];
                    valores[i][j] = 0;
                    this.ady(i,j-1);
                    this.ady(i,j);
                    j--;
                    movimiento = true;
                }else {
                    j++;
                }
            }
        }
        if (movimiento)
        	PosicionarNumAleatorio();
        System.out.println("MOVIENDO A LA IZQUIERDA\n");
        return 0;
    }


    public int moverArriba(){
        System.out.println("MOVIENDO ARRIBA\n");
        boolean movimiento = false;
        for(int j = 0; j < COLUMNAS; j++){
            int i = 1;
            while(i < FILAS){
                if(existeArista(i*4+j,(i-1)*4+j) && valores[i][j] != 0){
                    valores[i-1][j] += valores[i][j];
                    valores[i][j] = 0;
                    this.ady(i-1,j);
                    this.ady(i,j);
                    i--;
                    movimiento = true;
                }else {
                    i++;
                }
            }
        }
        if (movimiento)
        	PosicionarNumAleatorio();
        return 0;
    }


    public int moverAbajo(){
        System.out.println("MOVIENDO ABAJO\n");
        boolean movimiento = false;
        for(int j = 0; j < COLUMNAS; j++){
            int i = 2;
            while(i >= 0){
                if(existeArista(i*4+j,(i+1)*4+j) && valores[i][j] != 0){
                    valores[i+1][j] += valores[i][j];
                    valores[i][j] = 0;
                    this.ady(i+1,j);
                    this.ady(i,j);
                    i++;
                    movimiento = true;
                }else {
                    i--;
                }
            }
        }
        if (movimiento)
        	PosicionarNumAleatorio();
        return 0;
    }

    
    public void PosicionarNumAleatorio() {
		Random random = new Random();
		int i, j;
		boolean posicionEncontrada = false;

		// Buscar una posición vacía aleatoria en el tablero
		while (!posicionEncontrada) { // mientras la posición no sea encontrada debe seguir con el bucle.
			i = random.nextInt(FILAS);
			j = random.nextInt(COLUMNAS);

			if (valores[i][j] == 0) {
				// Si la posición está vacía, agrega un valor aleatorio 2 o 4
				agregarValor(i, j, generarNumeroRandom2o4());
				posicionEncontrada = true; // Termina el bucle
			}
		}
	}
    
    private int generarNumeroRandom2o4() {

		Random random = new Random();
		int numrandom = random.nextInt(2);
		int result = (numrandom == 0) ? 2 : 4; // Si randomNumber es igual a 0, entonces result será igual a 2. si no es
												// igual a 0, entonces result será igual a 4.

		return result;
	}
    
    public boolean haPerdido() {
        // Comprobar si no hay movimientos posibles
        for (int f = 0; f < FILAS; f++) {
            for (int c = 0; c < COLUMNAS; c++) {
                if (valores[f][c] == 0) {
                    return false; // Todavía hay espacios vacíos, no ha perdido
                }
                if (f < FILAS - 1 && valores[f][c] == valores[f + 1][c]) {
                    return false; // Hay dos celdas adyacentes en la misma columna con el mismo valor, no ha perdido
                }
                if (c < COLUMNAS - 1 && valores[f][c] == valores[f][c + 1]) {
                    return false; // Hay dos celdas adyacentes en la misma fila con el mismo valor, no ha perdido
                }
            }
        }
        return true; // No se encontraron movimientos posibles, ha perdido
    }


	public int[][] getTablero() {
		int [][] _tablero = this.valores;
		if (haPerdido())
			System.out.println("PERDISTEE");
		return _tablero;
	}
}
