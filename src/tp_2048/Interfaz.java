package tp_2048;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Interfaz {
    private JFrame frame;
    private JPanel boardPanel;
    private JLabel[][] boardLabels;
    private static final int SIZE = 4;

    public Interfaz() {
        frame = new JFrame("2048");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());

        boardPanel = new JPanel();
        boardPanel.setLayout(new GridLayout(SIZE, SIZE));
        frame.getContentPane().add(boardPanel, BorderLayout.CENTER);

        frame.setVisible(true);
    }

    public void setBoard(int[][] board) {
        boardLabels = new JLabel[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                boardLabels[i][j] = new JLabel();
                boardLabels[i][j].setHorizontalAlignment(SwingConstants.CENTER);
                boardLabels[i][j].setOpaque(true);
                boardLabels[i][j].setBackground(Color.LIGHT_GRAY);
                boardLabels[i][j].setFont(new Font("Arial", Font.BOLD, 24));
                boardPanel.add(boardLabels[i][j]);
            }
        }
        updateBoard(board);
    }

    public void updateBoard(int[][] board) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                boardLabels[i][j].setText(board[i][j] == 0 ? "" : String.valueOf(board[i][j]));
                Color color;
                switch (board[i][j]) {
                    case 0: color = Color.LIGHT_GRAY; break;
                    case 2: color = new Color(0xEEE4DA); break;
                    case 4: color = new Color(0xEDE0C8); break;
                    case 8: color = new Color(0xF2B179); break;
                    case 16: color = new Color(0xF59563); break;
                    case 32: color = new Color(0xF67C5F); break;
                    case 64: color = new Color(0xF65E3B); break;
                    case 128: color = new Color(0xEDCF72); break;
                    case 256: color = new Color(0xEDCC61); break;
                    case 512: color = new Color(0xEDC850); break;
                    case 1024: color = new Color(0xEDC53F); break;
                    case 2048: color = new Color(0xEDC22E); break;
                    default: color = Color.BLACK;
                }
                boardLabels[i][j].setBackground(color);
            }
        }
    }

    public void addKeyListener(KeyListener listener) {
        frame.addKeyListener(listener);
    }
}
