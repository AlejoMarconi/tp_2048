package tp_2048;

public class Logica {
    private int[][] board;
    private static final int SIZE = 4;

    public Logica() {
        board = new int[SIZE][SIZE];
        initializeBoard();
    }

    private void initializeBoard() {
        addNewTile();
        addNewTile();
    }

    private void addNewTile() {
        int value = Math.random() < 0.9 ? 2 : 4;
        int x, y;
        do {
            x = (int) (Math.random() * SIZE);
            y = (int) (Math.random() * SIZE);
        } while (board[x][y] != 0);
        board[x][y] = value;
    }

    public int[][] getBoard() {
        return board;
    }

    public void moveLeft() {
        boolean moved = false;
        for (int i = 0; i < SIZE; i++) {
            moved |= mergeTilesInRow(i);
            moved |= shiftTilesInRow(i);
        }
        if (moved) addNewTile();
    }

    public void moveRight() {
        boolean moved = false;
        for (int i = 0; i < SIZE; i++) {
            reverseRow(i);
            moved |= mergeTilesInRow(i);
            moved |= shiftTilesInRow(i);
            reverseRow(i);
        }
        if (moved) addNewTile();
    }

    public void moveUp() {
        boolean moved = false;
        for (int i = 0; i < SIZE; i++) {
            moved |= mergeTilesInColumn(i);
            moved |= shiftTilesInColumn(i);
        }
        if (moved) addNewTile();
    }

    public void moveDown() {
        boolean moved = false;
        for (int i = 0; i < SIZE; i++) {
            reverseColumn(i);
            moved |= mergeTilesInColumn(i);
            moved |= shiftTilesInColumn(i);
            reverseColumn(i);
        }
        if (moved) addNewTile();
    }

    private boolean mergeTilesInRow(int row) {
        boolean moved = false;
        for (int i = 0; i < SIZE - 1; i++) {
            if (board[row][i] == 0) continue;
            if (board[row][i] == board[row][i + 1]) {
                board[row][i] *= 2;
                board[row][i + 1] = 0;
                moved = true;
            }
        }
        return moved;
    }

    private boolean shiftTilesInRow(int row) {
        boolean moved = false;
        for (int i = 0; i < SIZE - 1; i++) {
            if (board[row][i] == 0) {
                for (int j = i + 1; j < SIZE; j++) {
                    if (board[row][j] != 0) {
                        board[row][i] = board[row][j];
                        board[row][j] = 0;
                        moved = true;
                        break;
                    }
                }
            }
        }
        return moved;
    }

    private boolean mergeTilesInColumn(int col) {
        boolean moved = false;
        for (int i = 0; i < SIZE - 1; i++) {
            if (board[i][col] == 0) continue;
            if (board[i][col] == board[i + 1][col]) {
                board[i][col] *= 2;
                board[i + 1][col] = 0;
                moved = true;
            }
        }
        return moved;
    }

    private boolean shiftTilesInColumn(int col) {
        boolean moved = false;
        for (int i = 0; i < SIZE - 1; i++) {
            if (board[i][col] == 0) {
                for (int j = i + 1; j < SIZE; j++) {
                    if (board[j][col] != 0) {
                        board[i][col] = board[j][col];
                        board[j][col] = 0;
                        moved = true;
                        break;
                    }
                }
            }
        }
        return moved;
    }

    private void reverseRow(int row) {
        int start = 0;
        int end = SIZE - 1;
        while (start < end) {
            int temp = board[row][start];
            board[row][start] = board[row][end];
            board[row][end] = temp;
            start++;
            end--;
        }
    }

    private void reverseColumn(int col) {
        int start = 0;
        int end = SIZE - 1;
        while (start < end) {
            int temp = board[start][col];
            board[start][col] = board[end][col];
            board[end][col] = temp;
            start++;
            end--;
        }
    }

}
