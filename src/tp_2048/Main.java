package tp_2048;

import javax.swing.*;
import java.awt.event.*;

public class Main {
    private Interfaz interfaz;
    private Logica logica;

    public Main() {
        interfaz = new Interfaz();
        logica = new Logica();
        interfaz.setBoard(logica.getBoard());

        interfaz.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_LEFT:
                        logica.moveLeft();
                        break;
                    case KeyEvent.VK_RIGHT:
                        logica.moveRight();
                        break;
                    case KeyEvent.VK_UP:
                        logica.moveUp();
                        break;
                    case KeyEvent.VK_DOWN:
                        logica.moveDown();
                        break;
                }
                interfaz.updateBoard(logica.getBoard());
            }
        });
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Main();
            }
        });
    }
}
