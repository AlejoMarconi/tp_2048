import javax.swing.*;

import interfazJuego.Interfaz;
import controlador.ControladorJuego;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Interfaz interfaz = new Interfaz();
                ControladorJuego controlador = new ControladorJuego(interfaz);
                interfaz.setControlador(controlador);

                interfaz.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(KeyEvent e) {
                        controlador.manejarEvento(e.getKeyCode());
                    }
                });
            }
        });
    }
}